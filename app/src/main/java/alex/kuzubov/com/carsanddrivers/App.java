package alex.kuzubov.com.carsanddrivers;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

/**
 * Created by kuzya on 7/15/17.
 */

public class App extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();
    }

    public static Context getAppContext(){
        return mContext;
    }
}
