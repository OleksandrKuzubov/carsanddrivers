package alex.kuzubov.com.carsanddrivers.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import alex.kuzubov.com.carsanddrivers.App;
import alex.kuzubov.com.carsanddrivers.R;
import alex.kuzubov.com.carsanddrivers.holders.CarHolder;
import alex.kuzubov.com.carsanddrivers.holders.DriverHolder;
import alex.kuzubov.com.carsanddrivers.holders.MainHolder;
import alex.kuzubov.com.carsanddrivers.interfaces.IItemsChoiseListener;
import alex.kuzubov.com.carsanddrivers.interfaces.ISelectedListener;
import alex.kuzubov.com.carsanddrivers.interfaces.ItemActionListener;
import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.model.Entity;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 * MainAdapter class that used for show cars and drivers content for user
 */

public class MainAdapter<T extends Entity> extends RecyclerView.Adapter<MainHolder> {

    private static final int STEP = 2;
    private Context mContext;
    private LayoutInflater mInflator;
    private IItemsChoiseListener mItemsListener;
    private List<T> mData;
    private ItemActionListener mActionListener;
    private List<T> mSelectedItems;
    private boolean isSeletecMode;

    public MainAdapter(ItemActionListener listener){
        init();
        mActionListener = listener;
    }

    public MainAdapter(IItemsChoiseListener listener){
        init();
        mItemsListener = listener;
        mSelectedItems = new ArrayList<>();
    }

    private void init() {
        mContext = App.getAppContext();
        mInflator = LayoutInflater.from(mContext);
    }

    @Override
    public MainHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MainHolder holder = null;
        View view;
        switch (viewType){
            case Car.CAR_TYPE:
                view = mInflator.inflate(R.layout.car_holder_layout, null);
                holder = new CarHolder(view, mActionListener);
                break;
            case Driver.DRIVER_TYPE:
                view = mInflator.inflate(R.layout.driver_holder_layout, null);
                holder = new DriverHolder(view, mActionListener);
                break;
            case Car.SELECTED_CAR_TYPE:
                view = mInflator.inflate(R.layout.car_selected_layout, null);
                holder = new CarHolder(view, mSelectedListener);
                break;
            case Driver.SELECTED_DRIVER_TYPE:
                view = mInflator.inflate(R.layout.driver_selected_layout, null);
                holder = new DriverHolder(view, mSelectedListener);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(MainHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return isSeletecMode ? mData.get(position).getItemType() + STEP : mData.get(position).getItemType();
    }

    /**
     * Fill adapter with data that incomes
     */
    public void fillData(List<T> data) {
        mData = data;
        notifyDataSetChanged();
    }

    /**
     * Delete special item from adapter
     */
    public void deleteItem(Entity item) {
        int index = mData.indexOf(item);
        mData.remove(item);
        notifyItemRemoved(index);
    }

    /**
     * Set selection mode for, that need to use adapter inside dialogs where was implemented
     * selection logic
     */
    public void setSeletecMode(boolean seletecMode) {
        isSeletecMode = seletecMode;
    }

    /**
     * Listener for selected items in selection mode
     */
    private ISelectedListener mSelectedListener = new ISelectedListener<T>() {
        @Override
        public void onItemSelected(T item) {
            mSelectedItems.add(item);
        }

        @Override
        public void onItemUnselected(T item) {
            mSelectedItems.remove(item);
        }
    };

    public List<T> getmSelectedItems() {
        return mSelectedItems;
    }
}
