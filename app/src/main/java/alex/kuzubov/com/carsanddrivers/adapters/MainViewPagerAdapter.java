package alex.kuzubov.com.carsanddrivers.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import alex.kuzubov.com.carsanddrivers.R;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.ui.fragments.CarsFragment;
import alex.kuzubov.com.carsanddrivers.ui.fragments.DriversFragment;
import alex.kuzubov.com.carsanddrivers.ui.fragments.MainFragment;

/**
 * Created by kuzya on 7/15/17.
 */

/**
 * Main viewpager adapter
 */
public class MainViewPagerAdapter extends FragmentStatePagerAdapter {

    private static int PAGE_COUNT = 2;
    private final Context mContext;
    private List<MainFragment> mFragmentList;

    public MainViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
        fillFragments();
    }

    private void fillFragments() {
        mFragmentList = new ArrayList<>();
        mFragmentList.add(CarsFragment.newInstance());
        mFragmentList.add(DriversFragment.newInstance());
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0:
                title = mContext.getString(R.string.cars_tab_title);
                    break;
            case 1:
                title = mContext.getString(R.string.drivers_tab_title);
                break;
        }
        return title;
    }
}
