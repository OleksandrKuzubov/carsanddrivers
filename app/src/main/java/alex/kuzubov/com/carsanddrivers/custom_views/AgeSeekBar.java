package alex.kuzubov.com.carsanddrivers.custom_views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

/**
 * Created by kuzya on 7/17/17.
 */

/**
 * Custom seekbar, needed for show age header above thumb upon moving
 */

public class AgeSeekBar extends android.support.v7.widget.AppCompatSeekBar {
    private Paint mPaint;
    private static final float THUMB_TEXT_SIZE = 30;

    public AgeSeekBar(Context context) {
        super(context);
        mPaint = new Paint();
        mPaint.setTextSize(THUMB_TEXT_SIZE);
    }

    public AgeSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint();
        mPaint.setTextSize(THUMB_TEXT_SIZE);
    }

    public AgeSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mPaint = new Paint();
        mPaint.setTextSize(THUMB_TEXT_SIZE);
    }

    @Override
    protected void onDraw(Canvas c) {
        super.onDraw(c);
        int thumb_x = (int) this.getThumb().getBounds().exactCenterX();
        int middle = (int) THUMB_TEXT_SIZE;
        c.drawText("Age " + getProgress(), thumb_x, middle, mPaint);
    }
}
