package alex.kuzubov.com.carsanddrivers.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import alex.kuzubov.com.carsanddrivers.App;
import alex.kuzubov.com.carsanddrivers.db.CarCursorWrapper;
import alex.kuzubov.com.carsanddrivers.db.CarsDriversDBScheme;
import alex.kuzubov.com.carsanddrivers.db.DbHelper;
import alex.kuzubov.com.carsanddrivers.db.DriverCursorWrapper;
import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 * Class for interact with data from db
 */
public class DataLab {

    private static DataLab mRef;
    private final Context mContext;
    private final SQLiteDatabase mDatabase;

    private DataLab(Context context) {
        mContext = context;
        mDatabase = new DbHelper(mContext).getWritableDatabase();
    }

    public static DataLab getInstance() {
        if (mRef == null) {
            mRef = new DataLab(App.getAppContext());
        }

        return mRef;
    }

    public static ContentValues getContentValues(Car car) {
        ContentValues values = new ContentValues();
        values.put(CarsDriversDBScheme.CarTable.Cols.BRAND, car.getBrand());
        values.put(CarsDriversDBScheme.CarTable.Cols.MODEL, car.getModel());
        values.put(CarsDriversDBScheme.CarTable.Cols.YEAR, car.getYear());
        values.put(CarsDriversDBScheme.CarTable.Cols.ICON, car.getIconUrl());
        return values;
    }

    public static ContentValues getContentValues(Driver driver) {
        ContentValues values = new ContentValues();
        values.put(CarsDriversDBScheme.DriverTable.Cols.NAME, driver.getName());
        values.put(CarsDriversDBScheme.DriverTable.Cols.SURNAME, driver.getSurname());
        values.put(CarsDriversDBScheme.DriverTable.Cols.AGE, driver.getAge());
        values.put(CarsDriversDBScheme.DriverTable.Cols.LEVEL, driver.getLevel());
        values.put(CarsDriversDBScheme.DriverTable.Cols.ICON, driver.getIconUrl());
        return values;
    }

    /**
     *Get all cars from db
     */
    public List<Car> getCars() {
        List<Car> cars = new ArrayList<>();
        CarCursorWrapper cursor = queryCars(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                cars.add(cursor.getCar());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return cars;
    }

    /**
     *Get all cars from db for specific driver
     */
    public List<Car> getCars(int id) {
        List<Car> cars = new ArrayList<>();
        CarCursorWrapper cursor = queryCarsForDriver(id);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                cars.add(cursor.getCar());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return cars;
    }

    private CarCursorWrapper queryCars(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                CarsDriversDBScheme.CarTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new CarCursorWrapper(cursor);
    }

    private CarCursorWrapper queryCarsForDriver(int driverId) {
        String selectQuery = "SELECT * FROM " + CarsDriversDBScheme.CarTable.NAME + " cr, "
                + CarsDriversDBScheme.CarDriverTable.NAME + " cd " +
                "WHERE cd." + CarsDriversDBScheme.CarDriverTable.Cols.DRIVER_REF + " = ?"
                + " AND cd." + CarsDriversDBScheme.CarDriverTable.Cols.CAR_REF + " = "
                + "cr." + CarsDriversDBScheme.CarTable.Cols._ID;


        Cursor cursor = mDatabase.rawQuery(selectQuery, new String[]{driverId + ""});
        return new CarCursorWrapper(cursor);
    }

    private DriverCursorWrapper queryDriversForCar(int carId) {
        String selectQuery = "SELECT * FROM " + CarsDriversDBScheme.DriverTable.NAME + " cr, "
                + CarsDriversDBScheme.CarDriverTable.NAME + " cd " +
                "WHERE cd." + CarsDriversDBScheme.CarDriverTable.Cols.CAR_REF + " = ?"
                + " AND cd." + CarsDriversDBScheme.CarDriverTable.Cols.DRIVER_REF + " = "
                + "cr." + CarsDriversDBScheme.DriverTable.Cols._ID;


        Cursor cursor = mDatabase.rawQuery(selectQuery, new String[]{carId + ""});
        return new DriverCursorWrapper(cursor);
    }

    /**
     *Get all drivers from db
     */
    public List<Driver> getDrivers() {
        List<Driver> drivers = new ArrayList<>();
        DriverCursorWrapper cursor = queryDrivers(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                drivers.add(cursor.getDriver());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return drivers;
    }

    /**
     *Get all drivers from db for specific car
     */
    public List<Driver> getDrivers(int carId) {
        List<Driver> drivers = new ArrayList<>();
        DriverCursorWrapper cursor = queryDriversForCar(carId);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                drivers.add(cursor.getDriver());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return drivers;
    }

    private DriverCursorWrapper queryDrivers(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                CarsDriversDBScheme.DriverTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new DriverCursorWrapper(cursor);
    }

    /**
     *Save car to db
     */
    public Long saveCarItem(Car car) {
        return mDatabase.insert(CarsDriversDBScheme.CarTable.NAME, null, getContentValues(car));
    }

    /**
     *Save driver to db
     */
    public Long saveDriverItem(Driver driver) {
        return mDatabase.insert(CarsDriversDBScheme.DriverTable.NAME, null, getContentValues(driver));
    }

    /**
     *Delete car from db
     */
    public int deleteCar(int id) {
        mDatabase.delete(
                CarsDriversDBScheme.CarDriverTable.NAME,
                CarsDriversDBScheme.CarDriverTable.Cols.CAR_REF + " = ?",
                new String[]{id + ""});
        mDatabase.delete(
                CarsDriversDBScheme.CarTable.NAME,
                CarsDriversDBScheme.CarTable.Cols._ID + " = ?",
                new String[]{id + ""});

        return 1;
    }

    /**
     *Delete drivers from db
     */
    public Integer deleteDriver(int id) {
        mDatabase.delete(
                CarsDriversDBScheme.CarDriverTable.NAME,
                CarsDriversDBScheme.CarDriverTable.Cols.DRIVER_REF + " = ?",
                new String[]{id + ""});
        mDatabase.delete(
                CarsDriversDBScheme.DriverTable.NAME,
                CarsDriversDBScheme.DriverTable.Cols._ID + " = ?",
                new String[]{id + ""});

        return 1;
    }

    /**
     *Update car in db
     */
    public int updateCar(Car car) {
        return mDatabase.update(
                CarsDriversDBScheme.CarTable.NAME,
                getContentValues(car),
                CarsDriversDBScheme.CarTable.Cols._ID + " = ?",
                new String[]{car.getId() + ""});
    }

    /**
     *Update driver in db
     */
    public int updateDriver(Driver driver) {
        return mDatabase.update(
                CarsDriversDBScheme.DriverTable.NAME,
                getContentValues(driver),
                CarsDriversDBScheme.DriverTable.Cols._ID + " = ?",
                new String[]{driver.getId() + ""});
    }

    /**
     *Merge drivers to specific car
     */
    public Boolean bindDrivers(List<Driver> drivers, Car car) {
        for(Driver driver : drivers){
            mDatabase.insert(CarsDriversDBScheme.CarDriverTable.NAME, null, getContentValues(car.getId(), driver.getId()));
        }
        return true;
    }

    /**
     *Merge cars to specific driver
     */
    public Boolean bindCars(List<Car> cars, Driver driver) {
        for(Car car : cars){
            mDatabase.insert(CarsDriversDBScheme.CarDriverTable.NAME, null, getContentValues(car.getId(), driver.getId()));
        }
        return true;
    }

    private ContentValues getContentValues(int carId, int driverId) {
        ContentValues values = new ContentValues();
        values.put(CarsDriversDBScheme.CarDriverTable.Cols.CAR_REF, carId);
        values.put(CarsDriversDBScheme.CarDriverTable.Cols.DRIVER_REF, driverId);
        return values;
    }
}
