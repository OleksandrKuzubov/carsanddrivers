package alex.kuzubov.com.carsanddrivers.db;

import android.database.Cursor;
import android.database.CursorWrapper;

import alex.kuzubov.com.carsanddrivers.model.Car;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Helper class to get car model from cursor
 */

public class CarCursorWrapper extends CursorWrapper {
    public CarCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Car getCar() {
        Car car = new Car();
        car.setId(getInt(getColumnIndex(CarsDriversDBScheme.CarTable.Cols._ID)));
        car.setIconUrl(getString(getColumnIndex(CarsDriversDBScheme.CarTable.Cols.ICON)));
        car.setBrand(getString(getColumnIndex(CarsDriversDBScheme.CarTable.Cols.BRAND)));
        car.setModel(getString(getColumnIndex(CarsDriversDBScheme.CarTable.Cols.MODEL)));
        car.setYear(getInt(getColumnIndex(CarsDriversDBScheme.CarTable.Cols.YEAR)));
        return car;
    }
}
