package alex.kuzubov.com.carsanddrivers.db;

import android.provider.BaseColumns;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Helper class that contain data for create db
 */
public class CarsDriversDBScheme {
    public static final class CarTable{
        public static final String NAME = "CarInfo";

        public static final class Cols implements BaseColumns {
            public static final String ICON = "icon";
            public static final String BRAND = "brand";
            public static final String MODEL = "model";
            public static final String YEAR = "year";
        }
    }

    public static final class DriverTable{
        public static final String NAME = "DriverInfo";

        public static final class Cols implements BaseColumns{
            public static final String ICON = "icon";
            public static final String NAME = "name";
            public static final String SURNAME = "surname";
            public static final String LEVEL = "level";
            public static final String AGE = "age";
        }
    }

    public static final class CarDriverTable {
        public static final String NAME = "CarDriverInfo";

        public static final class Cols implements BaseColumns{
            public static final String CAR_REF = "car_ref";
            public static final String DRIVER_REF = "driver_ref";
        }
    }
}
