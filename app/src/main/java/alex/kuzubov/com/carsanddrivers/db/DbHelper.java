package alex.kuzubov.com.carsanddrivers.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import alex.kuzubov.com.carsanddrivers.data.DataLab;
import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;


/**
 * Created by kuzya on 7/15/17.
 */

/**
 *Helper class to create and init db
 */

public class DbHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "carsdrivers.db";

    private static final String DATABASE_CREATE_TABLE_CARS =
            "create table " + CarsDriversDBScheme.CarTable.NAME + " ("
                    + CarsDriversDBScheme.CarTable.Cols._ID + " integer primary key autoincrement, "
                    + CarsDriversDBScheme.CarTable.Cols.BRAND + " string, "
                    + CarsDriversDBScheme.CarTable.Cols.MODEL + " string, "
                    + CarsDriversDBScheme.CarTable.Cols.ICON + " string, "
                    + CarsDriversDBScheme.CarTable.Cols.YEAR + " integer );";

    private static final String DATABASE_CREATE_TABLE_DRIVERS =
            "create table " + CarsDriversDBScheme.DriverTable.NAME + " ("
                    + CarsDriversDBScheme.DriverTable.Cols._ID + " integer primary key autoincrement, "
                    + CarsDriversDBScheme.DriverTable.Cols.NAME + " string, "
                    + CarsDriversDBScheme.DriverTable.Cols.SURNAME + " string, "
                    + CarsDriversDBScheme.DriverTable.Cols.ICON + " string, "
                    + CarsDriversDBScheme.DriverTable.Cols.AGE + " integer, "
                    + CarsDriversDBScheme.DriverTable.Cols.LEVEL + " integer ); ";

    private static final String DATABASE_CREATE_TABLE_CARS_DRIVERS =
            "create table " + CarsDriversDBScheme.CarDriverTable.NAME + " ("
                    + CarsDriversDBScheme.CarDriverTable.Cols._ID + " integer primary key autoincrement, "
                    + CarsDriversDBScheme.CarDriverTable.Cols.CAR_REF + " integer, "
                    + CarsDriversDBScheme.CarDriverTable.Cols.DRIVER_REF + " integer," +
                    "UNIQUE(" + CarsDriversDBScheme.CarDriverTable.Cols.CAR_REF +
                    "," + CarsDriversDBScheme.CarDriverTable.Cols.DRIVER_REF + ") ON CONFLICT IGNORE ); ";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_CREATE_TABLE_CARS);
        sqLiteDatabase.execSQL(DATABASE_CREATE_TABLE_DRIVERS);
        sqLiteDatabase.execSQL(DATABASE_CREATE_TABLE_CARS_DRIVERS);

        initDb(sqLiteDatabase);
    }

    /**
     *Fill db with initial data
     */
    private void initDb(SQLiteDatabase sqLiteDatabase) {
        for (Driver driver : prepareDriverList()) {
            sqLiteDatabase.insert(CarsDriversDBScheme.DriverTable.NAME, null, DataLab.getContentValues(driver));
        }
        for (Car car : prepareCarList()) {
            sqLiteDatabase.insert(CarsDriversDBScheme.CarTable.NAME, null, DataLab.getContentValues(car));
        }
    }

    /**
     *Prepare default list of drivers
     */
    private List<Driver> prepareDriverList() {
        List<Driver> drivers = new ArrayList<>();
        drivers.add(new Driver("John", "Galt", 43, 5));
        drivers.add(new Driver("John", "Balt", 42, 4));
        drivers.add(new Driver("Gohn", "Dalt", 41, 3));
        drivers.add(new Driver("Bidon", "Silver", 40, 2));
        drivers.add(new Driver("Mike", "Tayson", 39, 1));
        drivers.add(new Driver("Roody", "Doody", 38, 5));
        drivers.add(new Driver("John", "Travolta", 37, 4));
        drivers.add(new Driver("Selena", "Gomes", 36, 3));
        drivers.add(new Driver("Anna", "Marie", 35, 2));
        drivers.add(new Driver("Yan", "Ivanov", 34, 1));
        drivers.add(new Driver("Prosto", "Petya", 33, 5));
        return drivers;
    }

    /**
     *Prepare default list of cars
     */
    private List<Car> prepareCarList() {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("Audi", "A5", 2012));
        cars.add(new Car("BMW", "M5", 2014));
        cars.add(new Car("Audi", "Q7", 2010));
        cars.add(new Car("Mersedes", "E-CLASS", 2015));
        cars.add(new Car("DODGE", "T-MODEL", 2007));
        cars.add(new Car("OPEL", "ASTRA", 2006));
        cars.add(new Car("BENTLEY", "MOOLSAN", 2014));
        cars.add(new Car("LADA", "KALINA", 2012));
        cars.add(new Car("LADA", "VESTA", 2016));
        cars.add(new Car("Porche", "PANAMERA", 2013));
        cars.add(new Car("Mersedes", "A-CLASS", 2012));
        return cars;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CarsDriversDBScheme.CarTable.NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CarsDriversDBScheme.DriverTable.NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CarsDriversDBScheme.CarDriverTable.NAME);
        onCreate(sqLiteDatabase);
    }
}
