package alex.kuzubov.com.carsanddrivers.db;

import android.database.Cursor;
import android.database.CursorWrapper;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Helper class to get driver model from cursor
 */

public class DriverCursorWrapper extends CursorWrapper {
    public DriverCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Driver getDriver(){
        Driver driver = new Driver();
        driver.setId(getInt(getColumnIndex(CarsDriversDBScheme.DriverTable.Cols._ID)));
        driver.setIconUrl(getString(getColumnIndex(CarsDriversDBScheme.DriverTable.Cols.ICON)));
        driver.setName(getString(getColumnIndex(CarsDriversDBScheme.DriverTable.Cols.NAME)));
        driver.setSurname(getString(getColumnIndex(CarsDriversDBScheme.DriverTable.Cols.SURNAME)));
        driver.setAge(getInt(getColumnIndex(CarsDriversDBScheme.DriverTable.Cols.AGE)));
        driver.setLevel(getInt(getColumnIndex(CarsDriversDBScheme.DriverTable.Cols.LEVEL)));
        return driver;
    }
}
