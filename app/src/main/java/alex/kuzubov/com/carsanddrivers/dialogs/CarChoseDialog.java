package alex.kuzubov.com.carsanddrivers.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.mvp.drivers_screen.presenter.DriverPresenter;
import alex.kuzubov.com.carsanddrivers.mvp.drivers_screen.view.IDriverView;
import alex.kuzubov.com.carsanddrivers.ui.activities.BaseDetailActivity;

/**
 * Created by kuzya on 7/18/17.
 */

/**
 *Dialog to choose drivers for specific car
 */
public class CarChoseDialog extends MultiChoiceItemDialog implements IDriverView {

    private DriverPresenter mPresenter;

    public static CarChoseDialog newInstance(Car item) {

        Bundle args = new Bundle();

        args.putSerializable(BaseDetailActivity.KEY_ITEM, item);

        CarChoseDialog fragment = new CarChoseDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new DriverPresenter(this);
    }

    @Override
    protected void initContent() {
        super.initContent();
        mPresenter.loadData();
    }


    @Override
    public void fillData(List<Driver> drivers) {
        mAdapter.fillData(drivers);
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(getContext(), "Oooooopps!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeletedSuccess() {}

    @Override
    public void onBindItemsSuccess() {}

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}
