package alex.kuzubov.com.carsanddrivers.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.mvp.cars_screen.presenter.CarPresenter;
import alex.kuzubov.com.carsanddrivers.mvp.cars_screen.view.ICarView;
import alex.kuzubov.com.carsanddrivers.ui.activities.BaseDetailActivity;

/**
 * Created by kuzya on 7/18/17.
 */

/**
 *Dialog to choose cars for specific drivers
 */
public class DriverChoiseDialog extends MultiChoiceItemDialog implements ICarView{
    private CarPresenter mPresenter;

    public static DriverChoiseDialog newInstance(Driver item) {

        Bundle args = new Bundle();

        args.putSerializable(BaseDetailActivity.KEY_ITEM, item);

        DriverChoiseDialog fragment = new DriverChoiseDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new CarPresenter(this);
    }

    @Override
    protected void initContent() {
        super.initContent();
        mPresenter.loadData();
    }


    @Override
    public void fillData(List<Car> cars) {
        mAdapter.fillData(cars);
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(getContext(), "Oooooopps!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeletedSuccess() {}

    @Override
    public void onBindItemsSuccess() {}

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}
