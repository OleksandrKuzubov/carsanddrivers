package alex.kuzubov.com.carsanddrivers.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import alex.kuzubov.com.carsanddrivers.R;
import alex.kuzubov.com.carsanddrivers.adapters.MainAdapter;
import alex.kuzubov.com.carsanddrivers.interfaces.IItemsChoiseListener;
import alex.kuzubov.com.carsanddrivers.model.Entity;
import alex.kuzubov.com.carsanddrivers.ui.activities.BaseDetailActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kuzya on 7/17/17.
 */

/**
 *Main choose dialog
 */

public class MultiChoiceItemDialog extends DialogFragment{

    public static final String TAG = MultiChoiceItemDialog.class.getSimpleName();

    @BindView(R.id.dialog_recycler_view)
    RecyclerView mRecyclerView;

    protected MainAdapter mAdapter;
    protected Entity mItem;

    private IItemsChoiseListener mItemsListener;

    public void setItemListener(IItemsChoiseListener mItemListener) {
        this.mItemsListener = mItemListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mItem = (Entity) getArguments().getSerializable(BaseDetailActivity.KEY_ITEM);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.multi_choise_dialog_layout, null);
        ButterKnife.bind(this, view);
        initContent();

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.chose_dialog_title)
                .setView(view)
                .setPositiveButton(getResources().getString(android.R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mItemsListener.onItemsSelected(mAdapter.getmSelectedItems(), mItem);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(getResources().getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
    }

    protected void initContent() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new MainAdapter(mItemsListener);
        mAdapter.setSeletecMode(true);
        mRecyclerView.setAdapter(mAdapter);
    }
}
