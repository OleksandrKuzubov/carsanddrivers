package alex.kuzubov.com.carsanddrivers.holders;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import alex.kuzubov.com.carsanddrivers.R;
import alex.kuzubov.com.carsanddrivers.interfaces.ISelectedListener;
import alex.kuzubov.com.carsanddrivers.interfaces.ItemActionListener;
import alex.kuzubov.com.carsanddrivers.model.Car;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Optional;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Holder that draw car item info
 */
public class CarHolder extends MainHolder<Car> {

    @BindView(R.id.car_image_icon)
    ImageView mIcon;
    @BindView(R.id.car_brand_text)
    TextView mBrand;
    @BindView(R.id.car_model_text)
    TextView mModel;
    @Nullable
    @BindView(R.id.counter_container)
    FrameLayout mCounterContainer;
    @Nullable
    @BindView(R.id.counter)
    TextView mCounter;

    public CarHolder(View itemView, ItemActionListener mActionListener) {
        super(itemView, mActionListener);
        ButterKnife.bind(this, itemView);
    }

    public CarHolder(View itemView, ISelectedListener mSelectedListener) {
        super(itemView, mSelectedListener);
        ButterKnife.bind(this, itemView);
    }

    /**
     * Bind content to holder
     * @param item Model of car item
     * */
    @Override
    public void bind(Car item) {
        mItem = item;
        Picasso.with(itemView.getContext())
                .load(item.getIconUrl())
                .placeholder(R.mipmap.car_icon)
                .error(R.mipmap.car_icon)
                .fit()
                .centerCrop()
                .into(mIcon);

        mBrand.setText(item.getBrand());
        mModel.setText(item.getModel());

        checkSelection();

        setupCounter(item);
    }

    private void setupCounter(Car item) {
        if(mCounter == null || mCounterContainer == null) return;
        if(item.getDrivers() != null && !item.getDrivers().isEmpty()){
            mCounterContainer.setVisibility(View.VISIBLE);
            mCounter.setText(item.getDrivers().size() + "");
        } else {
            mCounterContainer.setVisibility(View.GONE);
        }
    }
}
