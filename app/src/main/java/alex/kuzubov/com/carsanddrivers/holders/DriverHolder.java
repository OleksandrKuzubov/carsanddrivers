package alex.kuzubov.com.carsanddrivers.holders;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import alex.kuzubov.com.carsanddrivers.R;
import alex.kuzubov.com.carsanddrivers.interfaces.ISelectedListener;
import alex.kuzubov.com.carsanddrivers.interfaces.ItemActionListener;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Holder that draw driver item info
 */

public class DriverHolder extends MainHolder<Driver> {

    @BindView(R.id.driver_image_icon)
    ImageView mIcon;
    @BindView(R.id.driver_name_text)
    TextView mDriverName;
    @BindView(R.id.driver_age_text)
    TextView mDriverAge;

    @Nullable
    @BindView(R.id.counter_container)
    FrameLayout mCounterContainer;
    @Nullable
    @BindView(R.id.counter)
    TextView mCounter;

    public DriverHolder(View itemView, ItemActionListener mActionListener) {
        super(itemView, mActionListener);
        ButterKnife.bind(this, itemView);
    }

    public DriverHolder(View itemView, ISelectedListener mSelectedListener){
        super(itemView, mSelectedListener);
        ButterKnife.bind(this, itemView);
    }

    /**
     * Bind content to holder
     * @param item Model of driver item
     * */
    @Override
    public void bind(Driver item) {
        mItem = item;
        Picasso.with(itemView.getContext())
                .load(item.getIconUrl())
                .placeholder(R.mipmap.user_icon)
                .error(R.mipmap.user_icon)
                .fit()
                .centerCrop()
                .into(mIcon);

        mDriverName.setText(item.getName() + " " + item.getSurname());
        mDriverAge.setText(itemView.getContext().getString(R.string.age_string_format, item.getAge()));

        checkSelection();

        setupCounter(item);
    }

    private void setupCounter(Driver item) {
        if(mCounter == null || mCounterContainer == null) return;
        if(item.getCars() != null && !item.getCars().isEmpty()){
            mCounterContainer.setVisibility(View.VISIBLE);
            mCounter.setText(item.getCars().size() + "");
        } else {
            mCounterContainer.setVisibility(View.GONE);
        }
    }
}
