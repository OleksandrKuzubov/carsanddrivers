package alex.kuzubov.com.carsanddrivers.holders;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import alex.kuzubov.com.carsanddrivers.R;
import alex.kuzubov.com.carsanddrivers.interfaces.ISelectedListener;
import alex.kuzubov.com.carsanddrivers.interfaces.ItemActionListener;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.model.Entity;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Base holder for item
 */
public abstract class MainHolder<T> extends RecyclerView.ViewHolder implements
        View.OnLongClickListener, View.OnClickListener{

    private ISelectedListener mSelectedListener;
    protected ItemActionListener mListener;
    protected Entity mItem;

    public MainHolder(View itemView, ItemActionListener mActionListener) {
        super(itemView);
        mListener = mActionListener;
        itemView.setOnLongClickListener(this);
    }

    public MainHolder(View itemView, ISelectedListener mActionListener) {
        super(itemView);
        mSelectedListener = mActionListener;
        itemView.setOnClickListener(this);
    }

    @Optional
    @OnClick(R.id.detail_icon)
    public void onDetailClick(){
        mListener.onDetailClicked(mItem);
    }

    @Optional
    @OnClick(R.id.trash_icon)
    public void onTrashClick(){
        mListener.onTrashClicked(mItem);
    }

    @Override
    public boolean onLongClick(View view) {
        if(mListener != null){
            mListener.onItemLongClicked(mItem);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View view) {
          if(mItem.isSelected()){
              mSelectedListener.onItemUnselected(mItem);
              itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), android.R.color.white));
              mItem.setSelected(false);
          } else {
              mSelectedListener.onItemSelected(mItem);
              itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), android.R.color.holo_blue_light));
              mItem.setSelected(true);
          }
    }

    protected void checkSelection() {
        if(!mItem.isSelected()){
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), android.R.color.white));
        } else {
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), android.R.color.holo_blue_light));
        }
    }

    public abstract void bind(T item);
}
