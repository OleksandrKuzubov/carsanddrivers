package alex.kuzubov.com.carsanddrivers.interfaces;

/**
 * Created by kuzya on 7/17/17.
 */

/**
 *Listener for confirm action upon delete item
 */
public interface IConfirmListener {
    void onDeleteConfirm();
}
