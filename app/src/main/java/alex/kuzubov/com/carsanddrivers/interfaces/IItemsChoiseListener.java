package alex.kuzubov.com.carsanddrivers.interfaces;

import android.sax.EndElementListener;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.model.Entity;

/**
 * Created by kuzya on 7/18/17.
 */

/**
 *Listener that notify when user merge item with each other
 */

public interface IItemsChoiseListener<T extends Entity, R extends Entity> {
    void onItemsSelected(List<R> items, T item);
}
