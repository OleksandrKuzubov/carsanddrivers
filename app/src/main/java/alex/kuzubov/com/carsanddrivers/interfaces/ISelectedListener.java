package alex.kuzubov.com.carsanddrivers.interfaces;

import alex.kuzubov.com.carsanddrivers.model.Entity;

/**
 * Created by kuzya on 7/18/17.
 */

/**
 *Listener for check selection upon multy chose
 */
public interface ISelectedListener<T extends Entity> {
    void onItemSelected(T item);
    void onItemUnselected(T item);
}
