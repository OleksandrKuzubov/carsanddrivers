package alex.kuzubov.com.carsanddrivers.interfaces;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Interface to get specific type from model
 */

public interface ITypedItem {

    int getItemType();
}
