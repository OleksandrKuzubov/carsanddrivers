package alex.kuzubov.com.carsanddrivers.interfaces;

import alex.kuzubov.com.carsanddrivers.model.Entity;

/**
 * Created by kuzya on 7/17/17.
 */

/**
 *Listener for user iteraction inside holder
 */

public interface ItemActionListener<T extends Entity> {
    void onTrashClicked(T item);
    void onDetailClicked(T item);
    void onItemLongClicked(T item);
}
