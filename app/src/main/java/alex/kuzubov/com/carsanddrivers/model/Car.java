package alex.kuzubov.com.carsanddrivers.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Car model
 */

public class Car extends Entity implements Serializable{

    public static final int CAR_TYPE = 1;
    public static final int SELECTED_CAR_TYPE = 3;

    private String mBrand;
    private String mModel;
    private int mYear;//Year of issue
    private int mDriverRef;
    private List<Driver> mDrivers;

    public Car(String mBrand, String mModel, int mYear) {
        this.mBrand = mBrand;
        this.mModel = mModel;
        this.mYear = mYear;
    }

    public Car(){};

    public String getBrand() {
        return mBrand;
    }

    public void setBrand(String mBrand) {
        this.mBrand = mBrand;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String mModel) {
        this.mModel = mModel;
    }

    public int getYear() {
        return mYear;
    }

    public void setYear(int mYear) {
        this.mYear = mYear;
    }

    public int getDriverRef() {
        return mDriverRef;
    }

    public void setDriverRef(int mDriverRef) {
        this.mDriverRef = mDriverRef;
    }

    @Override
    public int getItemType() {
        return CAR_TYPE;
    }

    public List<Driver> getDrivers() {
        return mDrivers;
    }

    public void setDrivers(List<Driver> mDrivers) {
        this.mDrivers = mDrivers;
    }
}
