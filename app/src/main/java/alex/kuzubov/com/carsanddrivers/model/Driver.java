package alex.kuzubov.com.carsanddrivers.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Driver model
 */

public class Driver extends Entity implements Serializable{

    public static final int DRIVER_TYPE = 2;
    public static final int SELECTED_DRIVER_TYPE = 4;

    private String mName;
    private String mSurname;
    private int mAge;
    private int mLevel;
    private int mCarRef;
    private List<Car> mCars;

    public Driver(String mName, String mSurname, int mAge, int mLevel) {
        this.mName = mName;
        this.mSurname = mSurname;
        this.mAge = mAge;
        this.mLevel = mLevel;
    }

    public Driver(){};

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getSurname() {
        return mSurname;
    }

    public void setSurname(String mSurname) {
        this.mSurname = mSurname;
    }

    public int getAge() {
        return mAge;
    }

    public void setAge(int mAge) {
        this.mAge = mAge;
    }

    public int getLevel() {
        return mLevel;
    }

    public void setLevel(int mLevel) {
        this.mLevel = mLevel;
    }

    public int getCarRef() {
        return mCarRef;
    }

    public void setCarRef(int mCarRef) {
        this.mCarRef = mCarRef;
    }

    @Override
    public int getItemType() {
        return DRIVER_TYPE;
    }

    public List<Car> getCars() {
        return mCars;
    }

    public void setCars(List<Car> mCars) {
        this.mCars = mCars;
    }
}
