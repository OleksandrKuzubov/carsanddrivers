package alex.kuzubov.com.carsanddrivers.model;

import java.io.Serializable;

import alex.kuzubov.com.carsanddrivers.interfaces.ITypedItem;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Base model
 */
public class Entity implements ITypedItem, Serializable{

    protected int mId;
    protected String mIconUrl;
    protected boolean isSelected;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getIconUrl() {
        return mIconUrl;
    }

    public void setIconUrl(String mIconUrl) {
        this.mIconUrl = mIconUrl;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int getItemType() {
        return 0;
    }
}
