package alex.kuzubov.com.carsanddrivers.mvp;

import alex.kuzubov.com.carsanddrivers.data.DataLab;

/**
 * Created by kuzya on 7/17/17.
 */

/**
 *Base repository
 */
public class BaseModel {

    protected DataLab mDataLab;

    public BaseModel() {
        mDataLab = DataLab.getInstance();
    }
}
