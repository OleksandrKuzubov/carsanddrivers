package alex.kuzubov.com.carsanddrivers.mvp;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kuzya on 6/4/17.
 */

/**
 *Base busyness logic
 */
public class BasePresenter {

    private ObservableTransformer schedulersTransformer;
    private CompositeDisposable mCompositeDisposable;

    @SuppressWarnings("unchecked")
    protected  <T> ObservableTransformer<T, T> applySchedulers() {
        return (ObservableTransformer<T, T>) schedulersTransformer;
    }

    public BasePresenter() {
        this.schedulersTransformer = new ObservableTransformer() {
            @Override
            public ObservableSource apply(Observable upstream) {
                return upstream.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
            }
        };
        this.mCompositeDisposable = new CompositeDisposable();
    }

    public void onDestroy(){
        mCompositeDisposable.clear();
    }

    public void addSubscription(Disposable dis){
        mCompositeDisposable.add(dis);
    }
}
