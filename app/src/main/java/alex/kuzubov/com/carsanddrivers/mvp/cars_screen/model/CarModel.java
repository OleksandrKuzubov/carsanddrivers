package alex.kuzubov.com.carsanddrivers.mvp.cars_screen.model;

import java.util.List;
import java.util.concurrent.Callable;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.mvp.BaseModel;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;

/**
 * Created by kuzya on 7/16/17.
 */

public class CarModel extends BaseModel implements ICarModel{

    public CarModel(){
        super();
    }

    /**
     *Load data for car screen
     */
    @Override
    public Observable<List<Car>> loadData() {
        return Observable.fromCallable(new Callable<List<Car>>() {
            @Override
            public List<Car> call() throws Exception {
                return mDataLab.getCars();
            }
        }).flatMap(new Function<List<Car>, ObservableSource<Car>>() {
            @Override
            public ObservableSource<Car> apply(@NonNull List<Car> cars) throws Exception {
                return Observable.fromIterable(cars);
            }
        }).flatMap(new Function<Car, Observable<Car>>() {
            @Override
            public Observable<Car> apply(@NonNull Car car) throws Exception {
                return Observable.zip(Observable.just(car), getDrivers(car),
                        new BiFunction<Car, List<Driver>, Car>() {
                            @Override
                            public Car apply(@NonNull Car car, @NonNull List<Driver> drivers) throws Exception {
                                car.setDrivers(drivers);
                                return car;
                            }
                        });
            }
        }).toList().toObservable();
    }

    /**
     *Delete item from car screen
     */
    @Override
    public Observable<Integer> deleteItem(final int id) {
        return Observable.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return mDataLab.deleteCar(id);
            }
        });
    }

    /**
     *Merge drivers to specific car
     */
    @Override
    public Observable<Boolean> bindDrivers(final List<Driver> drivers, final Car car) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return mDataLab.bindDrivers(drivers, car);
            }
        });
    }

    /**
     *Get drivers for specific car
     */
    @Override
    public Observable<List<Driver>> getDrivers(final Car car) {
        return Observable.fromCallable(new Callable<List<Driver>>() {
            @Override
            public List<Driver> call() throws Exception {
                return mDataLab.getDrivers(car.getId());
            }
        });
    }
}
