package alex.kuzubov.com.carsanddrivers.mvp.cars_screen.model;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import io.reactivex.Observable;

/**
 * Created by kuzya on 7/16/17.
 */

public interface ICarModel {
    Observable<List<Car>> loadData();

    Observable<Integer> deleteItem(int id);

    Observable<Boolean> bindDrivers(List<Driver> drivers, Car car);

    Observable<List<Driver>> getDrivers(Car car);
}
