package alex.kuzubov.com.carsanddrivers.mvp.cars_screen.presenter;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.mvp.BasePresenter;
import alex.kuzubov.com.carsanddrivers.mvp.cars_screen.model.CarModel;
import alex.kuzubov.com.carsanddrivers.mvp.cars_screen.view.ICarView;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Presenter for car busyness logic
 */

public class CarPresenter extends BasePresenter implements ICarPresenter {

    private final ICarView mView;
    private final CarModel mModel;

    public CarPresenter(ICarView view) {
        mView = view;
        mModel = new CarModel();
    }

    /**
     *Load data async for car screen
     */
    @Override
    public void loadData() {
        Disposable dis = mModel.loadData()
                .compose(this.<List<Car>>applySchedulers())
                .subscribe(new Consumer<List<Car>>() {
                    @Override
                    public void accept(@NonNull List<Car> cars) throws Exception {
                        mView.fillData(cars);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mView.showError(throwable);
                    }
                });

        addSubscription(dis);
    }

    /**
     *Delete car async for car screen
     */
    @Override
    public void deleteCar(int id) {
        Disposable dis = mModel.deleteItem(id)
                .compose(this.<Integer>applySchedulers())
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(@NonNull Integer rows) throws Exception {
                        mView.onDeletedSuccess();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mView.showError(throwable);
                    }
                });

        addSubscription(dis);
    }

    /**
     *Merge drivers with car async
     */
    @Override
    public void bindDrivers(List<Driver> drivers, Car car) {
        Disposable dis = mModel.bindDrivers(drivers, car)
                .compose(this.<Boolean>applySchedulers())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(@NonNull Boolean cars) throws Exception {
                        mView.onBindItemsSuccess();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mView.showError(throwable);
                    }
                });

        addSubscription(dis);
    }
}
