package alex.kuzubov.com.carsanddrivers.mvp.cars_screen.presenter;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;

/**
 * Created by kuzya on 7/16/17.
 */

public interface ICarPresenter {
    void loadData();

    void deleteCar(int id);

    void bindDrivers(List<Driver> drivers, Car car);
}
