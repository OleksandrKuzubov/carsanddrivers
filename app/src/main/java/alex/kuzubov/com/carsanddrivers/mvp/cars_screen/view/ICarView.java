package alex.kuzubov.com.carsanddrivers.mvp.cars_screen.view;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.model.Car;

/**
 * Created by kuzya on 7/16/17.
 */

public interface ICarView {
    void fillData(List<Car> cars);

    void showError(Throwable throwable);

    void onDeletedSuccess();

    void onBindItemsSuccess();
}
