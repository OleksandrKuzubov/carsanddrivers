package alex.kuzubov.com.carsanddrivers.mvp.detail_screen.model;

import java.util.concurrent.Callable;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.mvp.BaseModel;
import io.reactivex.Observable;

/**
 * Created by kuzya on 7/17/17.
 */

/**
 *Repository for detail screen
 */

public class DetailModel extends BaseModel implements IDetailModel {

    public DetailModel() {
        super();
    }

    /**
     *Save car item
     */
    @Override
    public Observable<Long> saveCarItem(final Car car) {
        return Observable.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return mDataLab.saveCarItem(car);
            }
        });
    }

    /**
     *Save driver item
     */
    @Override
    public Observable<Long> saveDriverItem(final Driver driver) {
        return Observable.fromCallable(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return mDataLab.saveDriverItem(driver);
            }
        });

    }

    /**
     *Update driver item
     */
    @Override
    public Observable<Integer> updateDriver(final Driver driver) {
        return Observable.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return mDataLab.updateDriver(driver);
            }
        });
    }

    /**
     *Update car item
     */
    @Override
    public Observable<Integer> updateCarItem(final Car car) {
        return Observable.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return mDataLab.updateCar(car);
            }
        });
    }
}
