package alex.kuzubov.com.carsanddrivers.mvp.detail_screen.model;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import io.reactivex.Observable;

/**
 * Created by kuzya on 7/17/17.
 */

public interface IDetailModel {
    Observable<Long> saveCarItem(Car car);

    Observable<Long> saveDriverItem(Driver driver);

    Observable<Integer> updateDriver(Driver driver);

    Observable<Integer> updateCarItem(Car car);
}
