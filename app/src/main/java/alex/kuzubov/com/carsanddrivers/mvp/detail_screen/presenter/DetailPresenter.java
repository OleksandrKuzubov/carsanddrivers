package alex.kuzubov.com.carsanddrivers.mvp.detail_screen.presenter;

import alex.kuzubov.com.carsanddrivers.App;
import alex.kuzubov.com.carsanddrivers.R;
import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.mvp.BasePresenter;
import alex.kuzubov.com.carsanddrivers.mvp.detail_screen.model.DetailModel;
import alex.kuzubov.com.carsanddrivers.mvp.detail_screen.model.IDetailModel;
import alex.kuzubov.com.carsanddrivers.mvp.detail_screen.view.IDetailView;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by kuzya on 7/17/17.
 */

/**
 *Busyness logic for detail screen
 */
public class DetailPresenter extends BasePresenter implements IDetailPresenter {

    private IDetailView mView;
    private IDetailModel mModel;

    public DetailPresenter(IDetailView mView) {
        this.mView = mView;
        this.mModel = new DetailModel();
    }

    /**
     *Save car item async
     */
    @Override
    public void saveCarItem(Car car) {
        Disposable dis = mModel.saveCarItem(car)
                .compose(this.<Long>applySchedulers())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long id) throws Exception {
                        if(id != -1){
                            mView.onSaveItemSuccess(id);
                        } else {
                            mView.showError(new Throwable(App.getAppContext().getString(R.string.error_save)));
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mView.showError(throwable);
                    }
                });

        addSubscription(dis);
    }

    /**
     *Save driver item async
     */
    @Override
    public void saveDriverItem(Driver driver) {
        Disposable dis = mModel.saveDriverItem(driver)
                .compose(this.<Long>applySchedulers())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(@NonNull Long id) throws Exception {
                        if(id != -1){
                            mView.onSaveItemSuccess(id);
                        } else {
                            mView.showError(new Throwable(App.getAppContext().getString(R.string.error_save)));
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mView.showError(throwable);
                    }
                });

        addSubscription(dis);
    }

    /**
     *Update driver item async
     */
    @Override
    public void updateDriverItem(Driver driver) {
        Disposable dis = mModel.updateDriver(driver)
                .compose(this.<Integer>applySchedulers())
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(@NonNull Integer id) throws Exception {
                        if(id != -1){
                            mView.onUpdateItemSuccess();
                        } else {
                            mView.showError(new Throwable(App.getAppContext().getString(R.string.error_save)));
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mView.showError(throwable);
                    }
                });

        addSubscription(dis);
    }

    /**
     *Update car item async
     */
    @Override
    public void updateCarItem(Car car) {
        Disposable dis = mModel.updateCarItem(car)
                .compose(this.<Integer>applySchedulers())
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(@NonNull Integer id) throws Exception {
                        if(id != -1){
                            mView.onUpdateItemSuccess();
                        } else {
                            mView.showError(new Throwable(App.getAppContext().getString(R.string.error_save)));
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mView.showError(throwable);
                    }
                });

        addSubscription(dis);
    }
}
