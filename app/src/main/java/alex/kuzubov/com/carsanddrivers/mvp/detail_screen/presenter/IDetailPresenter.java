package alex.kuzubov.com.carsanddrivers.mvp.detail_screen.presenter;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;

/**
 * Created by kuzya on 7/17/17.
 */

public interface IDetailPresenter {
    void saveCarItem(Car car);

    void saveDriverItem(Driver driver);

    void updateDriverItem(Driver driver);

    void updateCarItem(Car car);
}
