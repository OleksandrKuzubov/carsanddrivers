package alex.kuzubov.com.carsanddrivers.mvp.detail_screen.view;

/**
 * Created by kuzya on 7/17/17.
 */

public interface IDetailView {
    void onSaveItemSuccess(Long id);

    void showError(Throwable throwable);

    void onUpdateItemSuccess();
}
