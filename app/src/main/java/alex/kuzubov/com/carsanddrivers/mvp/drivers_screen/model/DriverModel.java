package alex.kuzubov.com.carsanddrivers.mvp.drivers_screen.model;

import java.util.List;
import java.util.concurrent.Callable;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.mvp.BaseModel;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;

/**
 * Created by kuzya on 7/16/17.
 */

public class DriverModel extends BaseModel implements IDriverModel{

    public DriverModel() {
        super();
    }

    @Override
    public Observable<List<Driver>> loadData() {
        return Observable.fromCallable(new Callable<List<Driver>>() {
            @Override
            public List<Driver> call() throws Exception {
                return mDataLab.getDrivers();
            }
        }).flatMap(new Function<List<Driver>, ObservableSource<Driver>>() {
            @Override
            public ObservableSource<Driver> apply(@NonNull List<Driver> drivers) throws Exception {
                return Observable.fromIterable(drivers);
            }
        }).flatMap(new Function<Driver, Observable<Driver>>() {
            @Override
            public Observable<Driver> apply(@NonNull Driver driver) throws Exception {
                return Observable.zip(Observable.just(driver), getCars(driver),
                        new BiFunction<Driver, List<Car>, Driver>() {
                    @Override
                    public Driver apply(@NonNull Driver driver, @NonNull List<Car> cars) throws Exception {
                        driver.setCars(cars);
                        return driver;
                    }
                });
            }
        }).toList().toObservable();

    }

    @Override
    public Observable<Integer> deleteDriver(final int id) {
        return Observable.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return mDataLab.deleteDriver(id);
            }
        });
    }

    @Override
    public Observable<Boolean> bindCars(final List cars, final Driver driver) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return mDataLab.bindCars(cars, driver);
            }
        });
    }

    @Override
    public Observable<List<Car>> getCars(final Driver driver) {
        return Observable.fromCallable(new Callable<List<Car>>() {
            @Override
            public List<Car> call() throws Exception {
                return mDataLab.getCars(driver.getId());
            }
        });
    }
}
