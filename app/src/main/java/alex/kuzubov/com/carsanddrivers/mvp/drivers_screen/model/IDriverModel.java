package alex.kuzubov.com.carsanddrivers.mvp.drivers_screen.model;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import io.reactivex.Observable;

/**
 * Created by kuzya on 7/16/17.
 */

public interface IDriverModel {
    Observable<List<Driver>> loadData();

    Observable<Integer> deleteDriver(int id);

    Observable<Boolean> bindCars(List cars, Driver driver);

    Observable<List<Car>> getCars(Driver driver);
}
