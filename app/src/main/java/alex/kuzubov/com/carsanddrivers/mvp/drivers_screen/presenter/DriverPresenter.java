package alex.kuzubov.com.carsanddrivers.mvp.drivers_screen.presenter;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.mvp.BasePresenter;
import alex.kuzubov.com.carsanddrivers.mvp.drivers_screen.model.DriverModel;
import alex.kuzubov.com.carsanddrivers.mvp.drivers_screen.view.IDriverView;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by kuzya on 7/16/17.
 */

public class DriverPresenter extends BasePresenter implements IDriverPresenter {

    private final IDriverView mView;
    private final DriverModel mModel;

    public DriverPresenter(IDriverView view) {
        mView = view;
        mModel = new DriverModel();
    }

    @Override
    public void loadData() {
        Disposable dis = mModel.loadData()
                .compose(this.<List<Driver>>applySchedulers())
                .subscribe(new Consumer<List<Driver>>() {
                    @Override
                    public void accept(@NonNull List<Driver> drivers) throws Exception {
                        mView.fillData(drivers);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mView.showError(throwable);
                    }
                });

        addSubscription(dis);
    }

    @Override
    public void deleteDriver(int id) {
        Disposable dis = mModel.deleteDriver(id)
                .compose(this.<Integer>applySchedulers())
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(@NonNull Integer rows) throws Exception {
                        mView.onDeletedSuccess();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mView.showError(throwable);
                    }
                });

        addSubscription(dis);
    }

    @Override
    public void bindCars(List cars, Driver driver) {
        Disposable dis = mModel.bindCars(cars, driver)
                .compose(this.<Boolean>applySchedulers())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(@NonNull Boolean cars) throws Exception {
                        mView.onBindItemsSuccess();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        mView.showError(throwable);
                    }
                });

        addSubscription(dis);
    }
}
