package alex.kuzubov.com.carsanddrivers.mvp.drivers_screen.presenter;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.model.Driver;

/**
 * Created by kuzya on 7/16/17.
 */

public interface IDriverPresenter {
    void loadData();

    void deleteDriver(int id);

    void bindCars(List cars, Driver driver);
}
