package alex.kuzubov.com.carsanddrivers.mvp.drivers_screen.view;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.model.Driver;

/**
 * Created by kuzya on 7/16/17.
 */

public interface IDriverView {
    void fillData(List<Driver> drivers);

    void showError(Throwable throwable);

    void onDeletedSuccess();

    void onBindItemsSuccess();
}
