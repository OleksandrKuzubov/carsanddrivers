package alex.kuzubov.com.carsanddrivers.rest;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by kuzya on 6/4/17.
 */

public class NetworkConfig {

    public static final String BASE_URL = "https://baseurl.com/";

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}
