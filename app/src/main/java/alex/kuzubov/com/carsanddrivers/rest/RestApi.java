package alex.kuzubov.com.carsanddrivers.rest;

import alex.kuzubov.com.carsanddrivers.rest.model.CarInfo;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by kuzya on 6/4/17.
 */

public interface RestApi {

    /**
     * Get cars for app
     * @param id unique app id
     * @param version current app version
     * @return Cars objects with filled fields wrap to observable
     */
    @GET("cars.php")
    Observable<CarInfo> getCars(@Query("u") String id, @Query("v") String version);

    /**
     * Get drivers for app
     * @param id unique app id
     * @param version current app version
     * @return Drivers objects with filled fields wrap to observable
     */
    @GET("drivers.php")
    Observable<CarInfo> getDrivers(@Query("u") String id, @Query("v") String version);
}
