package alex.kuzubov.com.carsanddrivers.rest;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kuzya on 6/4/17.
 */

public class RestService {

    /**
     * Retrofit helper class for networking, that prepare network endpoint for work
     * */

    public static RestApi createSocialService() {

//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.addInterceptor(logging);  // <-- this is the important line!

        Retrofit.Builder retrofit = new Retrofit.Builder()
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .baseUrl(NetworkConfig.BASE_URL);

        return retrofit.build().create(RestApi.class);
    }
}
