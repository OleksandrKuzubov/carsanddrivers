package alex.kuzubov.com.carsanddrivers.rest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kuzya on 6/12/17.
 */

/**
 * Pojo model class to get object from JSON data
 */

public class CarInfo {

    @SerializedName("ip")
    @Expose
    private String ip;
    @SerializedName("port")
    @Expose
    private String port;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "CarInfo{" +
                "ip='" + ip + '\'' + "\n" +
                ", port='" + port + '\'' + "\n" +
                ", username='" + username + '\'' + "\n" +
                ", password='" + password + '\'' +
                '}';
    }
}
