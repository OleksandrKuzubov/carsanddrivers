package alex.kuzubov.com.carsanddrivers.rest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kuzya on 6/4/17.
 */

/**
 * Pojo model class to get object from JSON data
 */

public class SocialConfig{

    @SerializedName("trololo")
    @Expose
    private String value1;
    @SerializedName("adfadsf")
    @Expose
    private String value2;
    @SerializedName("jdsafa")
    @Expose
    private String value3;
    @SerializedName("dasfadsf")
    @Expose
    private CarInfo info;

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }

    public String getValue3() {
        return value3;
    }

    public void setValue3(String value3) {
        this.value3 = value3;
    }

    public CarInfo getInfo() {
        return info;
    }

    public void setInfo(CarInfo info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "SocialConfig{" +
                "value1='" + value1 + '\'' +
                ", value2='" + value2 + '\'' +
                ", value3='" + value3 + '\'' +
                ", info=" + info +
                '}';
    }
}

