package alex.kuzubov.com.carsanddrivers.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Toast;

import java.io.File;

import alex.kuzubov.com.carsanddrivers.mvp.detail_screen.presenter.DetailPresenter;
import alex.kuzubov.com.carsanddrivers.mvp.detail_screen.view.IDetailView;
import alex.kuzubov.com.carsanddrivers.util.RealPathUtil;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Base detail activity
 */
public abstract class BaseDetailActivity extends AppCompatActivity implements IDetailView {

    static final int PERMISSION_STORAGE = 1;
    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};

    protected static final int PICK_IMAGE_REQUEST = 1;
    public static final String KEY_ITEM = "BaseDetailActivity_Key_Item";
    protected Uri mImageCaptureUri;
    protected DetailPresenter mPresenter;

    protected void goToGallery() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(intent, PICK_IMAGE_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST
                && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            mImageCaptureUri = Uri.fromFile(new File(uriToFilename(uri)));
            updateUserIcon();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new DetailPresenter(this);
    }

    protected abstract void updateUserIcon();

    private String uriToFilename(Uri uri) {
        String path = null;

        if (Build.VERSION.SDK_INT < 11) {
            path = RealPathUtil.getRealPathFromURI_BelowAPI11(this, uri);
        } else if (Build.VERSION.SDK_INT < 19) {
            path = RealPathUtil.getRealPathFromURI_API11to18(this, uri);
        } else {
            path = RealPathUtil.getRealPathFromURI_API19(this, uri);
        }
        return path;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public void onSaveItemSuccess(Long id) {
        Toast.makeText(this, "Success saving item", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void showError(Throwable throwable) {
        Toast.makeText(this, "Fail saving item", Toast.LENGTH_SHORT).show();
    }

    protected int checkPosition(int year, String[] years) {
        int i = 0;
        for(String tmp : years){
            if(tmp.contains(year + "")) return i;
            i++;
        }
        return 0;
    }

    protected void checkIconAndUpdate(String iconPath) {
        if(iconPath != null && !TextUtils.isEmpty(iconPath)){
            mImageCaptureUri = Uri.parse(iconPath);
            updateUserIcon();
        }
    }

    @Override
    public void onUpdateItemSuccess() {
        Toast.makeText(this, "Success updating item", Toast.LENGTH_SHORT).show();
        finish();
    }

    /**
     * Helper method to check if permission already exist
     */
    public static boolean hasPermission(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }

            }
        }
        return true;
    }

    /**
     * Helper metod to check permission and enable it
     */
    protected void runtimePermission() {
        if (!hasPermission(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_STORAGE);
        } else {
            goToGallery();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    goToGallery();
                } else {
                    //permission denied
                }
                break;

        }
    }
}
