package alex.kuzubov.com.carsanddrivers.ui.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import alex.kuzubov.com.carsanddrivers.R;
import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.mvp.detail_screen.presenter.DetailPresenter;
import alex.kuzubov.com.carsanddrivers.mvp.detail_screen.view.IDetailView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Car detail activity for show and edit car info
 */

public class CarDetailActivity extends BaseDetailActivity {

    @BindView(R.id.car_detail_image_icon)
    CircleImageView mIconView;
    @BindView(R.id.brand_detail_edit_text)
    EditText mBrandView;
    @BindView(R.id.model_detail_edit_text)
    EditText mModelView;
    @BindView(R.id.year_detail_edit_text)
    Spinner mYearView;
    @BindView(R.id.car_detail_save_button)
    Button mSaveButton;
    private Car mCar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.car_detail_activity_layout);
        ButterKnife.bind(this);
        checkModeAndSetUpUI();
    }

    private void checkModeAndSetUpUI() {
        if(getIntent().hasExtra(KEY_ITEM)){
            mCar = (Car) getIntent().getSerializableExtra(KEY_ITEM);
            updateUI();
        } else {
            setUpSpinner(0);
        }
    }

    private void updateUI() {
        mBrandView.setText(mCar.getBrand());
        mModelView.setText(mCar.getModel());
        setUpSpinner(mCar.getYear());
        mSaveButton.setText(R.string.update_btn_text);
        checkIconAndUpdate(mCar.getIconUrl());
    }

    private void setUpSpinner(int year) {
        String[] years = getResources().getStringArray(R.array.spinner_years);
        int pos;
        if(year == 0){
            pos = 0;
        } else {
            pos = checkPosition(year, years);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, years);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mYearView.setAdapter(adapter);
        mYearView.setPrompt(getString(R.string.car_year_spinner));
        mYearView.setSelection(pos);
    }

    @OnTextChanged({R.id.brand_detail_edit_text, R.id.model_detail_edit_text})
    public void onTextChanged(){
        mBrandView.setError(null);
        mModelView.setError(null);
    }

    @OnClick(R.id.car_detail_image_icon)
    public void onCarDetailIconClick(ImageView view){
        runtimePermission();
    }

    @OnClick(R.id.car_detail_save_button)
    public void onSaveClick(View view){
        String brand = mBrandView.getText().toString();
        if(TextUtils.isEmpty(brand)){
            mBrandView.setError(getString(R.string.brand_error));
            return;
        }

        String model = mModelView.getText().toString();
        if(TextUtils.isEmpty(model)){
            mModelView.setError(getString(R.string.model_error));
            return;
        }

        int year = Integer.parseInt(((String)mYearView.getSelectedItem()).replaceAll("[\\D]",""));

        Car car = new Car(brand, model, year);
        if(mImageCaptureUri != null){
            car.setIconUrl(mImageCaptureUri.toString());
        }

        if(mCar != null){
            car.setId(mCar.getId());
            mPresenter.updateCarItem(car);
        } else {
            mPresenter.saveCarItem(car);
        }
    }

    @Override
    protected void updateUserIcon() {
        Picasso.with(this)
                .load(mImageCaptureUri)
                .placeholder(R.mipmap.car_icon)
                .error(R.mipmap.car_icon)
                .fit()
                .centerCrop()
                .into(mIconView);
    }
}
