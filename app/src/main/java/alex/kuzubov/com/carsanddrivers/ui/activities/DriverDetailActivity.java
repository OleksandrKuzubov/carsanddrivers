package alex.kuzubov.com.carsanddrivers.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.security.Permission;

import alex.kuzubov.com.carsanddrivers.R;
import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.util.RealPathUtil;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by kuzya on 7/16/17.
 */

/**
 *Driver detail activity for show and edit driver info
 */

public class DriverDetailActivity extends BaseDetailActivity {

    private static final String TAG = DriverDetailActivity.class.getSimpleName();

    @BindView(R.id.driver_detail_image_icon)
    CircleImageView mIconView;
    @BindView(R.id.name_detail_edit_text)
    EditText mNameView;
    @BindView(R.id.surname_detail_edit_text)
    EditText mSurnameView;
    @BindView(R.id.age_detail_edit_text)
    SeekBar mAgeBar;
    @BindView(R.id.level_detail_edit_text)
    Spinner mLevelView;
    @BindView(R.id.driver_detail_save_button)
    Button mSaveButton;

    private Driver mDriver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_detail_layout);
        ButterKnife.bind(this);
        checkModeAndSetUpUI();
    }

    private void checkModeAndSetUpUI() {
        if(getIntent().hasExtra(KEY_ITEM)){
            mDriver = (Driver) getIntent().getSerializableExtra(KEY_ITEM);
            updateUI();
        } else {
            setUpSpinner(0);
        }
    }

    private void updateUI() {
        setUpSpinner(mDriver.getLevel());
        mNameView.setText(mDriver.getName());
        mSurnameView.setText(mDriver.getSurname());
        mAgeBar.setProgress(mDriver.getAge());
        mSaveButton.setText(R.string.update_btn_text);
        checkIconAndUpdate(mDriver.getIconUrl());
    }

    @OnClick(R.id.driver_detail_image_icon)
    public void onDriveDetailIconClick(ImageView view){
        runtimePermission();
    }

    @Override
    protected void updateUserIcon() {
        Picasso.with(this)
                .load(mImageCaptureUri)
                .placeholder(R.mipmap.user_icon)
                .error(R.mipmap.user_icon)
                .fit()
                .centerCrop()
                .into(mIconView);
    }

    @OnTextChanged({R.id.name_detail_edit_text, R.id.surname_detail_edit_text})
    public void onTextChanged(){
        mNameView.setError(null);
        mSurnameView.setError(null);
    }

    @OnClick(R.id.driver_detail_save_button)
    public void onSaveClick(View view){
        String name = mNameView.getText().toString();
        if(TextUtils.isEmpty(name)){
            mNameView.setError("Incorrect name");
            return;
        }

        String surname = mSurnameView.getText().toString();
        if(TextUtils.isEmpty(surname)){
            mSurnameView.setError("Incorrect surname");
            return;
        }

        int level = Integer.parseInt(((String)mLevelView.getSelectedItem()).replaceAll("[\\D]",""));

        int age = mAgeBar.getProgress();

        Driver driver = new Driver(name, surname, age, level);
        if(mImageCaptureUri != null){
            driver.setIconUrl(mImageCaptureUri.toString());
        }

        if(mDriver != null){
            driver.setId(mDriver.getId());
            mPresenter.updateDriverItem(driver);
        } else {
            mPresenter.saveDriverItem(driver);
        }
    }

    private void setUpSpinner(int year) {
        String[] years = getResources().getStringArray(R.array.spinner_levels);
        int pos;
        if(year == 0){
            pos = 0;
        } else {
            pos = checkPosition(year, years);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, years);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mLevelView.setAdapter(adapter);
        mLevelView.setSelection(pos);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
