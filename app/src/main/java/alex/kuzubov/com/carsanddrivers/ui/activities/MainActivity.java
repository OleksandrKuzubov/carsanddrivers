package alex.kuzubov.com.carsanddrivers.ui.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import alex.kuzubov.com.carsanddrivers.R;
import alex.kuzubov.com.carsanddrivers.adapters.MainAdapter;
import alex.kuzubov.com.carsanddrivers.adapters.MainViewPagerAdapter;
import alex.kuzubov.com.carsanddrivers.ui.fragments.MainFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 *Main launcher activity
 */
public class MainActivity extends AppCompatActivity {

    public static final int CAR_POS = 0;
    public static final int DRIVER_POS = 1;

    @BindView(R.id.tablayout)
    TabLayout mTabs;
    @BindView(R.id.viewpager)
    ViewPager mViewPager;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private MainViewPagerAdapter mMainPagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        setUpContent();
    }

    @OnClick(R.id.fab)
    void onFabClick(View view) {
        startActivity(((MainFragment) mMainPagerAdapter.getItem(mViewPager.getCurrentItem())).getDetailIntent());
    }

    private void setUpContent() {
        mMainPagerAdapter = new MainViewPagerAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(mMainPagerAdapter);
        mTabs.setupWithViewPager(mViewPager);
    }

    /**
     *Notify fragment for update content
     */
    public void onBindItemSuccess(int pos) {
        ((MainFragment) mMainPagerAdapter.getItem(pos)).updateData();
    }
}
