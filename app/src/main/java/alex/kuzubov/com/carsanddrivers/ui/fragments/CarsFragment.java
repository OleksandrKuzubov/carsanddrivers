package alex.kuzubov.com.carsanddrivers.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.adapters.MainAdapter;
import alex.kuzubov.com.carsanddrivers.dialogs.CarChoseDialog;
import alex.kuzubov.com.carsanddrivers.dialogs.MultiChoiceItemDialog;
import alex.kuzubov.com.carsanddrivers.interfaces.IItemsChoiseListener;
import alex.kuzubov.com.carsanddrivers.interfaces.ItemActionListener;
import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.mvp.cars_screen.presenter.CarPresenter;
import alex.kuzubov.com.carsanddrivers.mvp.cars_screen.view.ICarView;
import alex.kuzubov.com.carsanddrivers.ui.activities.BaseDetailActivity;
import alex.kuzubov.com.carsanddrivers.ui.activities.CarDetailActivity;
import alex.kuzubov.com.carsanddrivers.ui.activities.MainActivity;

/**
 * Created by kuzya on 7/15/17.
 */

/**
 *Car fragment that show list of all cars and action with them
 */
public class CarsFragment extends MainFragment implements
        ICarView, ItemActionListener<Car>, IItemsChoiseListener<Car, Driver>{

    private static final String TAG = CarsFragment.class.getSimpleName();
    private CarPresenter mPresenter;

    public static CarsFragment newInstance() {

        Bundle args = new Bundle();

        CarsFragment fragment = new CarsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setUpAdapter() {
        mAdapter = new MainAdapter<Car>((ItemActionListener) this);
    }

    @Override
    public Intent getDetailIntent() {
        return new Intent(getContext(), CarDetailActivity.class);
    }

    @Override
    public void updateData() {
        mPresenter.loadData();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new CarPresenter(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.loadData();
    }

    @Override
    public void fillData(List<Car> cars) {
        if(cars != null && !cars.isEmpty()){
            mAdapter.fillData(cars);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public void showError(Throwable throwable) {
        Log.d(TAG, "Ooooopppss!", throwable);
    }

    @Override
    public void onDeletedSuccess() {
        Toast.makeText(getContext(), "Item was deleted successful", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBindItemsSuccess() {
        mPresenter.loadData();
        ((MainActivity)getActivity()).onBindItemSuccess(MainActivity.DRIVER_POS);
    }

    @Override
    public void onTrashClicked(Car item) {
        mCurrentItem = item;
        showConfirmDialog();
    }

    @Override
    public void onDetailClicked(Car item) {
        Intent intent = getDetailIntent();
        intent.putExtra(BaseDetailActivity.KEY_ITEM, item);
        startActivity(intent);
    }

    @Override
    public void onItemLongClicked(Car item) {
        showMultiChoiseDialog(item);
    }

    private void showMultiChoiseDialog(Car item) {
        MultiChoiceItemDialog dialog = CarChoseDialog.newInstance(item);
        dialog.setItemListener(this);
        dialog.show(getFragmentManager(), MultiChoiceItemDialog.TAG);
    }

    @Override
    public void onDeleteConfirm() {
        mAdapter.deleteItem(mCurrentItem);
        mPresenter.deleteCar(mCurrentItem.getId());
    }

    @Override
    public void onItemsSelected(List<Driver> drivers, Car car) {
        mPresenter.bindDrivers(drivers, car);
    }
}
