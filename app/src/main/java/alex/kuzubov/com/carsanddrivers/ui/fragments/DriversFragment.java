package alex.kuzubov.com.carsanddrivers.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import alex.kuzubov.com.carsanddrivers.adapters.MainAdapter;
import alex.kuzubov.com.carsanddrivers.dialogs.DriverChoiseDialog;
import alex.kuzubov.com.carsanddrivers.dialogs.MultiChoiceItemDialog;
import alex.kuzubov.com.carsanddrivers.interfaces.IItemsChoiseListener;
import alex.kuzubov.com.carsanddrivers.interfaces.ItemActionListener;
import alex.kuzubov.com.carsanddrivers.model.Car;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.model.Entity;
import alex.kuzubov.com.carsanddrivers.mvp.drivers_screen.presenter.DriverPresenter;
import alex.kuzubov.com.carsanddrivers.mvp.drivers_screen.view.IDriverView;
import alex.kuzubov.com.carsanddrivers.ui.activities.BaseDetailActivity;
import alex.kuzubov.com.carsanddrivers.ui.activities.DriverDetailActivity;
import alex.kuzubov.com.carsanddrivers.ui.activities.MainActivity;

/**
 * Created by kuzya on 7/15/17.
 */

/**
 *Car fragment that show list of all cars and action with them
 */
public class DriversFragment extends MainFragment implements IDriverView,
        ItemActionListener<Driver>, IItemsChoiseListener<Driver, Car> {

    private static final String TAG = DriversFragment.class.getSimpleName();

    private DriverPresenter mPresenter;

    public static DriversFragment newInstance() {

        Bundle args = new Bundle();

        DriversFragment fragment = new DriversFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setUpAdapter() {
        mAdapter = new MainAdapter<Driver>((ItemActionListener) this);
    }

    @Override
    public Intent getDetailIntent() {
        return new Intent(getContext(), DriverDetailActivity.class);
    }

    @Override
    public void updateData() {
        mPresenter.loadData();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new DriverPresenter(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.loadData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public void fillData(List<Driver> drivers) {
        if(drivers != null && !drivers.isEmpty()){
            mAdapter.fillData(drivers);
        }
    }

    @Override
    public void showError(Throwable throwable) {
        Log.e(TAG, "Oooopppss!!!", throwable);
    }

    @Override
    public void onDeletedSuccess() {
        Toast.makeText(getContext(), "Item was deleted successful", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBindItemsSuccess() {
        mPresenter.loadData();
        ((MainActivity)getActivity()).onBindItemSuccess(MainActivity.CAR_POS);
    }

    @Override
    public void onTrashClicked(Driver item) {
        mCurrentItem = item;
        showConfirmDialog();
    }

    @Override
    public void onDetailClicked(Driver item) {
        Intent intent = getDetailIntent();
        intent.putExtra(BaseDetailActivity.KEY_ITEM, item);
        startActivity(intent);
    }

    @Override
    public void onItemLongClicked(Driver item) {
        showMultiChoiseDialog(item);
    }

    private void showMultiChoiseDialog(Driver item) {
        MultiChoiceItemDialog dialog = DriverChoiseDialog.newInstance(item);
        dialog.setItemListener(this);
        dialog.show(getFragmentManager(), MultiChoiceItemDialog.TAG);
    }

    @Override
    public void onDeleteConfirm() {
        mAdapter.deleteItem(mCurrentItem);
        mPresenter.deleteDriver(mCurrentItem.getId());
    }

    @Override
    public void onItemsSelected(List<Car> cars, Driver driver) {
        mPresenter.bindCars(cars, driver);
    }
}
