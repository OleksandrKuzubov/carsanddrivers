package alex.kuzubov.com.carsanddrivers.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import alex.kuzubov.com.carsanddrivers.R;
import alex.kuzubov.com.carsanddrivers.adapters.MainAdapter;
import alex.kuzubov.com.carsanddrivers.dialogs.ConfirmDialog;
import alex.kuzubov.com.carsanddrivers.interfaces.IConfirmListener;
import alex.kuzubov.com.carsanddrivers.interfaces.ItemActionListener;
import alex.kuzubov.com.carsanddrivers.model.Driver;
import alex.kuzubov.com.carsanddrivers.model.Entity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kuzya on 7/15/17.
 */

/**
 *Base fragment for content
 */

public abstract class MainFragment extends Fragment implements IConfirmListener {

    @BindView(R.id.work_recycler_view)
    protected RecyclerView mRecyclerView;
    protected MainAdapter mAdapter;
    protected Entity mCurrentItem;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.work_fragment_layout, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initUI();
    }

    private void initUI() {
        if(mAdapter == null){
            setUpAdapter();
        }
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
    }

    protected void showConfirmDialog() {
        ConfirmDialog dialog = ConfirmDialog.newInstance();
        dialog.setListener(this);
        dialog.show(getFragmentManager(), ConfirmDialog.TAG);
    }

    public MainAdapter getAdapter(){
        return mAdapter;
    }

    public abstract void setUpAdapter();

    public abstract Intent getDetailIntent();

    public abstract void updateData();
}
